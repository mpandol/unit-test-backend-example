FROM node:10.15.0

WORKDIR /usr/src/app

ADD package*.json /usr/src/app/

RUN npm install

COPY . /usr/src/app
CMD ["npm","run","start"]
