import { EventEmitter } from 'events';
import MongoMemoryReplSet from 'mongodb-memory-server';
import mongoose from 'mongoose';

EventEmitter.defaultMaxListeners = Infinity;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 50000;

global.Array = Array;
global.Date = Date;
global.Function = Function;
global.Math = Math;
global.Number = Number;
global.Object = Object;
global.RegExp = RegExp;
global.String = String;
global.Uint8Array = Uint8Array;
global.WeakMap = WeakMap;
global.Set = Set;
global.Error = Error;
global.TypeError = TypeError;
global.parseInt = parseInt;
global.parseFloat = parseFloat;

let mongoServer;

beforeAll(async () => {
  /* mongo server */
  mongoServer = new MongoMemoryReplSet({
    debug: false,
    replSet: { storageEngine: 'wiredTiger' },
  });
  const mongoUri = await mongoServer.getConnectionString();

  /* mongoose */
  await mongoose.connect(mongoUri, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

afterEach(async () => {
  const { collections } = mongoose.connection;
  const promises = [];
  Object.keys(collections).forEach((collection) => {
    promises.push(collections[collection].deleteOne());
  });
  await Promise.all(promises);
});
