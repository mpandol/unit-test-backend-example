import Region from './model';

let region;
const regionExampleData = { name: 'Region test', description: 'Only for test purposes', code: 'RT' };

beforeEach(async () => {
  // ... auth and token code here
  region = await Region.create(regionExampleData);
});

describe('view', () => {
  it('returns simple view', () => {
    const view = region.view();
    expect(typeof view).toBe('object');
    expect(view.id).toBe(region.id);
    expect(view.name).toBe(region.name);
    expect(view.description).toBe(region.description);
    expect(view.code).toBe(region.code);
    expect(view.createdAt).toBeTruthy();
    expect(view.updatedAt).toBeTruthy();
  });

  it('returns full view', () => {
    const view = region.view(true);
    expect(typeof view).toBe('object');
    expect(view.id).toBe(region.id);
    expect(view.name).toBe(region.name);
    expect(view.description).toBe(region.description);
    expect(view.code).toBe(region.code);
    expect(view.createdAt).toBeTruthy();
    expect(view.updatedAt).toBeTruthy();
  });
});
