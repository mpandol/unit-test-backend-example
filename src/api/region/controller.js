import { success, notFound } from '../../services/response';
import Region from './model';

export const create = ({ bodymen: { body } }, res, next) => Region.create({ ...body })
  .then((region) => region.view(true))
  .then(success(res, 201))
  .catch(next);

export const index = ({ querymen: { query, select, cursor } }, res, next) => {
  Region.count(query)
    .then((count) => Region.find(query, select, cursor)
      .then((regions) => ({
        count,
        rows: regions.map((region) => region.view()),
      })))
    .then(success(res))
    .catch(next);
};

export const show = ({ params }, res, next) => Region.findById(params.id)
  .then(notFound(res))
  .then((region) => (region ? region.view() : null))
  .then(success(res))
  .catch(next);

export const update = ({ bodymen: { body }, params }, res, next) => Region.findById(params.id)
  .then(notFound(res))
  .then((region) => (region ? Object.assign(region, body).save() : null))
  .then((region) => (region ? region.view(true) : null))
  .then(success(res))
  .catch(next);

export const destroy = ({ params }, res, next) => Region.findById(params.id)
  .then(notFound(res))
  .then((region) => (region ? region.remove() : null))
  .then(success(res, 204))
  .catch(next);
