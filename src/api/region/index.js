import { Router } from 'express';
import { middleware as query } from 'querymen';
import { middleware as body } from 'bodymen';

// eslint-disable-next-line import/no-cycle
import {
  create, index, show, update, destroy,
} from './controller';
import { schema } from './model';

const router = new Router();
const {
  name, description, code,
} = schema.tree;

/**
 * @api {post} /regions Create region
 * @apiName CreateRegion
 * @apiGroup Region
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Region's name.
 * @apiParam description Region's description.
 * @apiParam code Region's code.
 * @apiSuccess {Object} region Region's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Region not found.
 * @apiError 401 user access only.
 */
router.post('/',
  body({
    name, description, code,
  }),
  create);

/**
 * @api {get} /regions Retrieve regions
 * @apiName RetrieveRegions
 * @apiGroup Region
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of regions.
 * @apiSuccess {Object[]} rows List of regions.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  query(),
  index);

/**
 * @api {get} /regions/:id Retrieve region
 * @apiName RetrieveRegion
 * @apiGroup Region
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} region Region's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Region not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  show);

/**
 * @api {put} /regions/:id Update region
 * @apiName UpdateRegion
 * @apiGroup Region
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Region's name.
 * @apiParam description Region's description.
 * @apiParam code Region's code.
 * @apiSuccess {Object} region Region's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Region not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  body({
    name, description, code,
  }),
  update);

/**
 * @api {delete} /regions/:id Delete region
 * @apiName DeleteRegion
 * @apiGroup Region
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Region not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  destroy);

export default router;
