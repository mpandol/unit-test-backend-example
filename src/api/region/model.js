import mongoose, { Schema } from 'mongoose';

const regionSchema = new Schema({
  name: {
    type: String,
    minlength: 3,
    maxlength: 50,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    trim: true,
    minlength: 3,
    maxlength: 100,
  },
  code: {
    type: String,
    minlength: 1,
    maxlength: 20,
    trim: true,
    required: true,
  },
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id; },
  },
});

regionSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      description: this.description,
      code: this.code,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };

    return full ? {
      ...view,
      // add properties for a full view
    } : view;
  },
};

const model = mongoose.model('Region', regionSchema);

export const { schema } = model;
export default model;
