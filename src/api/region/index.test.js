import request from 'supertest';

import routes from '.';
import Region from './model';
import { apiRoot } from '../../config';
import express from '../../services/express';

const app = () => express(apiRoot, routes);

let region;
const regionExampleData = { name: 'Region test', description: 'Only for test purposes', code: 'RT' };

beforeEach(async () => {
  // ... auth and token code here
  region = await Region.create(regionExampleData);
});

test('GET /regions 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`);
  expect(status).toBe(200);
  expect(Array.isArray(body.rows)).toBe(true);
  expect(Number.isNaN(body.count)).toBe(false);
});

test('GET /regions/:id 200', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${region.id}`);
  expect(status).toBe(200);
});

test('POST /regions 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send(regionExampleData);
  expect(status).toBe(201);
  expect(typeof body).toEqual('object');
  expect(body.name).toEqual(regionExampleData.name);
  expect(body.description).toEqual(regionExampleData.description);
  expect(body.code).toEqual(regionExampleData.code);
});

test('PUT /regions/:id 200', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${region.id}`)
    .send({ name: 'test', description: 'test', code: 'TT' });
  expect(status).toBe(200);
});

test('DELETE /regions/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${region.id}`);
  expect(status).toBe(204);
});
