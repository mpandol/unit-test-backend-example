import { Router } from 'express';
import { version } from '../../package.json';
import { env } from '../config';

import region from './region';

const router = new Router();

/**
 * @apiDefine master Master access onlyimport commune from './commune'
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**

/**
 * @api {get} / Retrieve api info
 * @apiName Retrieve object
 * @apiUse listParams
 * @apiSuccess {Object[]} rows List of api info.
 */
router.get('/', (request, response) => {
  if (request.url === '/' && request.method === 'GET') {
    response.status(200).json({
      state: 'up',
      version,
      env,
    });
    response.end();
  }
});

router.use('/regions', region);

export default router;
