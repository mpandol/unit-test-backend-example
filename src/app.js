import http from 'http';

import api from './api';
import express from './services/express';
import mongoose from './services/mongoose';

import {
  ip,
  env,
  port,
  mongo,
  apiRoot,
} from './config';

const app = express(apiRoot, api);
const server = http.createServer(app);

mongoose.connect(mongo.uri, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
});
mongoose.Promise = Promise;
mongoose.Query.setOptions = ({ lean: true });

setImmediate(async () => {
  server.listen(port, ip, () => {
    // eslint-disable-next-line no-console
    console.log('Express server listening on http://%s:%d, in %s mode', ip, port, env);
  });
});

export default app;
