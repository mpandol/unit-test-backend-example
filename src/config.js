import path from 'path';
import merge from 'lodash/merge';
import dotenv from 'dotenv-safe';

if (process.env.NODE_ENV === 'test') {
  dotenv.config({
    path: '.env',
    allowEmptyValues: true,
  });
}

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error(`You must set the ${name} environment variable`);
  }
  return process.env[name];
};

const hasEnvironmentVarsDefined = () => {
  if (process.env.NODE_ENV) {
    return true;
  }

  return false;
};

/* istanbul ignore next */
if (!hasEnvironmentVarsDefined()) {
  const envRes = dotenv.config({ allowEmptyValues: true });
  if (envRes.error) {
    // eslint-disable-next-line no-console
    console.log('Please define .env file, taking default values');
  } else {
    dotenv.load({
      path: path.join(__dirname, '../.env'),
      sample: path.join(__dirname, '../.env.example'),
      allowEmptyValues: true,
    });
  }
}

const emailNoReply = 'no-reply@unit-test-backend-example.com';

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 9000,
    ip: process.env.IP || '0.0.0.0',
    apiRoot: process.env.API_ROOT || '',
    defaultEmail: emailNoReply,
    masterKey: requireProcessEnv('MASTER_KEY'),
  },
  development: {
    mongo: {
      uri: process.env.MONGODB_DEVELOPMENT_URI || 'mongodb://localhost/unit-test-backend-example-dev',
      options: {
        useUnifiedTopology: true,
        debug: true,
      },
    },
    origin: process.env.ACCESS_CONTROL_ALLOW_ORIGIN || '*',
  },
  testing: {
    mongo: {
      uri: process.env.MONGODB_TESTING_URI || 'mongodb://localhost/unit-test-backend-example-test',
      options: {
        useUnifiedTopology: true,
        debug: true,
      },
    },
  },
  qa: {
    env: process.env.NODE_ENV || 'qa',
    port: process.env.PORT || 80,
    apiRoot: process.env.API_ROOT || '',
    defaultEmail: emailNoReply,
    masterKey: process.env.NODE_ENV ? requireProcessEnv('MASTER_KEY') : '',
    origin: process.env.ACCESS_CONTROL_ALLOW_ORIGIN || ['*'],
    mongo: {
      uri: process.env.MONGODB_QA_URI || 'mongodb://localhost/unit-test-backend-example-qa',
      options: {
        useUnifiedTopology: true,
        debug: true,
      },
    },
  },
  staging: {
    mongo: {
      uri: process.env.MONGODB_STAGING_URI || 'mongodb://localhost/unit-test-backend-example-stage',
      options: {
        useUnifiedTopology: true,
        debug: true,
      },
    },
  },
  production: {
    env: process.env.NODE_ENV || 'production',
    port: process.env.PORT || 80,
    apiRoot: process.env.API_ROOT || '',
    defaultEmail: emailNoReply,
    masterKey: process.env.NODE_ENV ? requireProcessEnv('MASTER_KEY') : '',
    origin: process.env.ACCESS_CONTROL_ALLOW_ORIGIN || ['*'],
    mongo: {
      uri: process.env.MONGODB_PRODUCTION_URI || 'mongodb://localhost/unit-test-backend-example-prod',
      options: {
        useUnifiedTopology: true,
        debug: false,
      },
    },
  },
};

export const ip = '';
export const env = '';
export const port = '';
export const mongo = '';
export const origin = '';
export const apiRoot = '';
export const masterKey = '';
export const defaultEmail = '';

module.exports = merge(config.all, config[config.all.env]);
export default module.exports;
